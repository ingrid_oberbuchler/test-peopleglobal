import React from 'react';

import './App.css';
import MainCanvas from "./components/MainCanvas";

const App = () => {

  return (
    <div className="app">
      <div className="main">
        <MainCanvas />
      </div>
    </div>
  );
}

export default App;
