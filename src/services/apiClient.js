import dataJsonFile from "./space.json"

const nodesJsonData = [
    { id: 0.1, label: 'Hex size 1', shape: 'hexagon', xHex: 0, yHex: 0, level: 1, size: 1 },
    { id: 0.2, label: 'Hex size 2', shape: 'hexagon', xHex: 1, yHex: 0, level: 1, size: 2 },
    { id: 0.3, label: 'Hex size 3', shape: 'hexagon', xHex: 2, yHex: 0, level: 1, size: 3 },
    { id: 1.0, label: 'Circle size 1', shape: 'circle', xHex: 0, yHex: 1, level: 1, size: 1 },
    { id: 1.2, label: 'Circle size 2', shape: 'circle', xHex: 1, yHex: 1, level: 1, size: 2 },
    { id: 1.3, label: 'Circle size 3', shape: 'circle', xHex: 2, yHex: 1, level: 1, size: 3 },
    { id: 1.4, label: 'Rect size 3', shape: 'rectangle', xHex: 4, yHex: 0, level: 1, size: 3 },
    { id: 1.5, label: 'Rect size 2', shape: 'rectangle', xHex: 5, yHex: 0, level: 1, size: 2 },
    { id: 1.6, label: 'Rect size 1', shape: 'rectangle', xHex: 6, yHex: 0, level: 1, size: 1 },
    { id: 1.7, label: 'Triangle size 3', shape: 'triangle', xHex: 1, yHex: 2, level: 1, size: 3 },
    { id: 1.8, label: 'Triangle size 3', shape: 'triangle', xHex: 2, yHex: 2, level: 1, size: 2 },
    { id: 1.9, label: 'Triangle size 3', shape: 'triangle', xHex: 3, yHex: 2, level: 1, size: 1 },
    { id: 4, label: 'Node 4', shape: 'hexagon', xHex: 4, yHex: 5, level: 1, size: 2 },
    { id: 5, label: '5', shape: 'rectangle', xHex: 9, yHex: 0, level: -1, size: 2 },
    { id: 6, label: '6', shape: 'triangle', xHex: 8, yHex: 2, level: -1, size: 3 },
    { id: 7, label: '7', shape: 'circle', xHex: 3, yHex: 3, level: -1, size: 1 },
    { id: 8, label: '8', shape: 'hexagon', xHex: 5, yHex: 5, level: -1, size: 3},
]

const getJsonData = () => {
     const convertDate = dataJsonFile.nodes.map(d => ({
         ...d,
        x: d.x + 580,
        y: d.y + 580,
        id: d.id,
        label: d.label,
        shape: d.shape,
        level: d.level || 0,
        size: d.size || 2
    }))

    return nodesJsonData.concat(convertDate)

}

const downloadJsonData = (data) => {
    dataJsonFile["nodes"] = data;
    return dataJsonFile
}


export { getJsonData, downloadJsonData }
