import React, {useCallback, useEffect, useRef, useState} from "react";
import {Layer, Stage, Group, Shape} from "react-konva";
import {defineGrid, extendHex} from "honeycomb-grid";

import {CANVAS_WIDTH, HEX_SIZE} from "./constants";
import {Shapes} from "./Shapes";
import {DragDropShapes} from "./DragDropShapes/DragDropShapes";
import {renderHexaGrid} from "./HexaGrid";
import {getJsonData, downloadJsonData} from '../services/apiClient';

// Define Hexagrid
export const Hex = extendHex({
    size: HEX_SIZE,
    orientation: 'pointy', // options: pointy, flat
})

export const Grid = defineGrid(Hex);

const levels = [-1, 0, 1]
const INIT_LEVEL = 0
let initialData = [];

const formatedData = () => {
    const newData = getJsonData();
    return newData.map(x => {
        // center of first hex
        const center = Hex(x.xHex, x.yHex).center()

        if (x.x && x.y) {
            // Load from import
            const hex = Grid.pointToHex(x.x, x.y);

            // pin to center of nearest hex
            const points = Hex(hex.x, hex.y).toPoint()

            return {
                ...x,
                x: points.x + center.x,
                y: points.y + center.y,
                xHex: hex.x,
                yHex: hex.y,
            }

        } else {
            // Load dummy data from custom initials
            const points = Hex(x.xHex, x.yHex).toPoint()
            return {
                ...x,
                x: points.x + center.x,
                y: points.y + center.y,
                xHex: x.xHex,
                yHex: x.yHex,
            }
        }
    })
}

export const MainCanvas = () => {
    const gridRef = useRef();
    const stageRef = useRef();
    const colRef = useRef();

    const [showGrid, setShowGrid ] = useState(true)
    const [outputNodes, setOutputNodes] = useState([])
    const [showLabelId, setShowLabelId] = useState(null);
    const [gridSize, setGridSize] = useState({ height: window.innerHeight})

    // On mount - first init
    const [data, setData] = useState([])
    useEffect(() => {
        initialData = formatedData();
        setData(initialData.filter(x => x.level === INIT_LEVEL))
    }, [])

    // Filter data on level changed
    const [level, setLevel] = useState(INIT_LEVEL)
    useEffect(() => {
        const filteredData = initialData.filter(x => x.level === level)
        setData(filteredData)
        setOutputNodes(filteredData)
    }, [level])

    // Set size based on col, TODO: move to separate hook, add resize function
    useEffect(() => {
        if (colRef) {
            setGridSize({
                width: colRef.current.clientWidth - 70,
                height: colRef.current.clientHeight
            })
        }
    }, [colRef])

    const handleZoom = value => {
        const increment = value === 'IN' ? +0.2 : -0.2
        const stage = stageRef.current.getStage()
        const gridLayer = gridRef.current.getLayer()
        const currentScale = gridLayer.getScale()

        gridLayer.scale({ x: currentScale.x+increment, y: currentScale.y+increment })
        stage.batchDraw()
    }

    const handleSetLevel = x => {
        setLevel(x)
    }

    const handleShowGrid = value => {
        setShowGrid(value)
    }

    const handleShowLabelId = (id) => (e) => {
        e.target.setAttrs({zIndex: 1})
        setShowLabelId(id)
    }

    const handleSceneGrid = (ctx) => {
        renderHexaGrid(ctx)
    }

    // TODO: move to custom hook
    // Find shape by ID and update position
    const onUpdateShape = useCallback((shape) => {
        const newData = data.map(x => x.id === shape.id ? shape : x)
        setData(newData)
        setOutputNodes(newData)
    }, [data])

    // Set shapes object
    const [addShape, setAddShape] = useState({});
    const onAddShape = (shape) => {
        setAddShape(shape)
    }

    // Push new shape to data
    const onNewShapeDrop = useCallback((e) => {
        // Register event position
        stageRef.current.setPointersPositions(e);

        // merge new object to Data
        setData(data.concat([{
            ...addShape,
            ...stageRef.current.getPointerPosition(),
            id: data.length+1, // we need uniq ID
        }]));

    }, [data, addShape])


    return (
        <>
            <div className="col"
                 onDrop={e => onNewShapeDrop(e)}
                 onDragOver={e => e.preventDefault()}
                 ref={colRef}
            >
                <Stage
                    width={gridSize?.width || CANVAS_WIDTH}
                    height={gridSize.height}
                    ref={stageRef}
                    draggable={true}
                >
                    <Layer>
                        <Group opacity={showGrid ? 1 : 0}>
                            <Shape
                                ref={gridRef}
                                x={0}
                                y={0}
                                width={gridSize?.width || CANVAS_WIDTH}
                                height={gridSize.height}
                                fill={'#dfdfdf'}
                                sceneFunc={handleSceneGrid}
                            />
                        </Group>
                        <Group>
                            {data.map((x, i) =>
                                <Shapes
                                    key={i}
                                    obj={x}
                                    updateShape={onUpdateShape}
                                    handleShowLabelId={handleShowLabelId}
                                    showLabelId={showLabelId}
                                />)}
                        </Group>
                    </Layer>
                    {/*<Layer ref={shapesRef}  scaleY={1} scaleX={1} x={CANVAS_WIDTH-50} y={0}>*/}
                    {/*</Layer>*/}
                </Stage>
                <div className="actions">
                    <svg className="zoomIn" onClick={() => handleZoom('IN')} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z" /><path d="M12 10h-2v2H9v-2H7V9h2V7h1v2h2v1z" /></svg>
                    <svg className="zoomOut" onClick={() => handleZoom('OUT')} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M15.5 14h-.79l-.28-.27C15.41 12.59 16 11.11 16 9.5 16 5.91 13.09 3 9.5 3S3 5.91 3 9.5 5.91 16 9.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14zM7 9h5v1H7z" /></svg>
                    <div className="showWrap">
                        {showGrid
                            ? <svg className="showGrid" onClick={() => handleShowGrid(false)} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0z" fill="none"/><path d="M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z"/></svg>
                            : <svg className="showGrid" onClick={() => handleShowGrid(true)} xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0zm0 0h24v24H0z" fill="none"/><path d="M12 7c2.76 0 5 2.24 5 5 0 .65-.13 1.26-.36 1.83l2.92 2.92c1.51-1.26 2.7-2.89 3.43-4.75-1.73-4.39-6-7.5-11-7.5-1.4 0-2.74.25-3.98.7l2.16 2.16C10.74 7.13 11.35 7 12 7zM2 4.27l2.28 2.28.46.46C3.08 8.3 1.78 10.02 1 12c1.73 4.39 6 7.5 11 7.5 1.55 0 3.03-.3 4.38-.84l.42.42L19.73 22 21 20.73 3.27 3 2 4.27zM7.53 9.8l1.55 1.55c-.05.21-.08.43-.08.65 0 1.66 1.34 3 3 3 .22 0 .44-.03.65-.08l1.55 1.55c-.67.33-1.41.53-2.2.53-2.76 0-5-2.24-5-5 0-.79.2-1.53.53-2.2zm4.31-.78l3.15 3.15.02-.16c0-1.66-1.34-3-3-3l-.17.01z"/></svg>
                        }
                    </div >
                    <div className="levelWrap">
                        <div>Level:</div>
                        {levels.map(x => <div className={level === x ? "levelBtn active" : "levelBtn"} onClick={() => { handleSetLevel(x) }} key={x}>{x}</div>)}
                    </div>
                    <div className="showWrap">
                        <DragDropShapes onDrop={onAddShape} />
                    </div>
                </div>
            </div>
            <div className="col">
                <div className="admin">
                    {level === 0 && <>
                    <h3 style={{color: '#e04c6c'}}>Osa x a y posunuta o<strong> 580</strong> px</h3>

                    {outputNodes.length > 0 && <div>
                        <a className="button--download button"
                           href={`data:text/json;charset=utf-8,${encodeURIComponent(
                               JSON.stringify(downloadJsonData(outputNodes))
                           )}`}
                           download="export.json"
                        >
                            {`Download export`}
                        </a>
                    </div>}

                    </>}

                    <pre><b>nodes:</b>{JSON.stringify(outputNodes, null, 2)}</pre>
                    {/*<pre><b>edges:</b>{JSON.stringify(connections, null, 2)}</pre> */}
                </div>
            </div>
        </>
    )
}

export default MainCanvas
