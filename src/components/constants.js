export const HEX_SIZE= 38;
export const CANVAS_WIDTH = 800;
export const CANVAS_HEIGHT = 1000;
export const H = Math.sqrt(3) * HEX_SIZE
export const S = 3 / 2 * HEX_SIZE
