import {Grid, Hex} from './MainCanvas'
import {H, S, HEX_SIZE} from './constants'

const drawCircle = (ctx, x, y, size) => {
    ctx.moveTo(x, y)
    ctx.beginPath();
    ctx.arc(x, y, size ? size : 4, 0, Math.PI * 2, false);
    ctx.fill();
    ctx.closePath();
}

export const renderHexaGrid = ctx => {
    ctx.strokeStyle = '#666';
    ctx.fillStyle = '#666';

    Grid.rectangle({ width: 30, height: 20, start: 0}).flatMap(hex => {
        const point = hex.toPoint();
        const center = Hex(point.x, point.y).center()
        const corners = hex.corners().map((corner) => corner.add(point));
        // separate the first from the other corners
        const [firstCorner, ...otherCorners] = corners;

        // move the "pen" to the first corner
        ctx.moveTo(firstCorner.x, firstCorner.y);
        ctx.beginPath();
        // draw lines to the other corners
        otherCorners.forEach(({ x, y }) => {
            ctx.lineTo(x, y)
        });

        // finish at the first corner
        ctx.lineTo(firstCorner.x, firstCorner.y);

        ctx.closePath();
        ctx.stroke();

        // Draw points
        corners.map(c => {
            const {x, y} = c;
            return drawCircle(ctx, x, y)
        })
        //
        // // Draw centered point of HEX
        drawCircle(ctx,point.x + center.x, point.y+center.y, 2)
        // // Draw inner hex points of rectangle
        drawCircle(ctx,point.x + (H / 3 * 2), point.y + (S - HEX_SIZE), 2)
        drawCircle(ctx,point.x + H / 3, point.y + (S - HEX_SIZE), 2)
        drawCircle(ctx,point.x + H / 3, point.y + S, 2)
        drawCircle(ctx,point.x + H / 3 * 2, point.y + S, 2)
        // // Draw inner hex points 2 points on cross-centered y
        drawCircle(ctx,point.x + (H / 6), point.y + center.y, 2)
        drawCircle(ctx,point.x + H / 6 * 5, point.y+center.y, 2)
    });
}

