import React from "react";
import {Circle, RegularPolygon, Rect, Label, Text, Tag} from "react-konva";
import Konva from "konva";

import {H, HEX_SIZE} from "./constants";
import { Hex, Grid } from "./MainCanvas";

const FILL = '#285df0'
const OPACITY = 0.8

const getSize = (size) => {
    return size === 3 ? HEX_SIZE : size === 2 ? (H / 3) : (H / 6)
}

const getRectProps = (obj) => {
    const hex = Hex(obj.x, obj.y)
    const center = Hex(obj.x, obj.y).center()

    switch (obj.size) {
        case 3: return {
            x: obj.x - center.x,
            y: obj.y - (center.y / 2),
            width: hex.width(),
            height: HEX_SIZE
        }
        case 2: return {
            x: obj.x - (center.x - H / 3),
            y: obj.y - (center.y / 2),
            width: (H / 3),
            height: HEX_SIZE,
        }
        case 1: return {
            x: obj.x - (H / 6),
            y: obj.y - (H / 12),
            width: (H / 3) ,
            height: (H / 6),
        }
        default: return {}
    }
}

const getTriangleProps = (obj) => {
    const hex = Hex(obj.x, obj.y).corners()
    const innerH = hex[3].y - hex[4].y
    switch (obj.size) {
        case 3: return {
            x: obj.x,
            y: obj.y,
            radius: HEX_SIZE
        }
        case 2: return {
            x: obj.x + (H / 6),
            y: obj.y - hex[4].y,
            radius: H / 3,
            rotation: -30
        }
        case 1: return {
            x: obj.x,
            y: obj.y - innerH / 3,
            radius: innerH / 3,
            rotation: 180
        }
        default: return {}
    }
}

const getHexProps = (obj) => {
    return {
        x: obj.x,
        y: obj.y,
        rotation: obj.size === 2 ? 30 : 0
    }
}

const getCenters = (obj) => {
    if (obj.shape === 'triangle') {
        return getTriangleProps(obj)
    }
    if (obj.shape === 'rectangle') {
        return getRectProps(obj)
    }
    if (obj.shape === 'hexagon') {
        return getHexProps(obj)
    }
}

const renderLabel = (obj, showLabelId) => {
    return (
        <Label
            x={obj.x}
            y={obj.y - 36}
            visible={showLabelId === obj.id}
        >
            <Tag
                fill='black'
                pointerDirection='down'
                pointerWidth={10}
                pointerHeight={5}
                shadowColor='#333'
                shadowBlur={6}
                shadowOffsetX={5}
                shadowOffsetY={5}
            />
            <Text
                text={obj.label}
                padding={5}
                fontSize={13}
                fill='white'
            />
        </Label>
    )
}

export const Shapes = ({obj, updateShape, handleShowLabelId, showLabelId}) => {

    const getShapeComponent = (obj) => {
        switch (obj.shape) {
            case ('circle'):
                return renderCircle(obj)
            case ('box'):
                return renderCircle(obj)
            case 'rectangle':
                return renderRectangle(obj)
            case 'triangle':
                return renderTriangle(obj)
            case 'hexagon':
                return renderHexagon(obj)
            default:
                return <></>
        }
    }

    const handleDragStart = e => {
        e.target.setAttrs({
            opacity: 0.5,
            scaleX: 1.1,
            scaleY: 1.1
        });
    }

    const handleDragEnd = (e, obj) => {
        const droppedPoint = { x: e.currentTarget.attrs.x, y: e.currentTarget.attrs.y }
        const hex = Grid.pointToHex(droppedPoint)
        const center = Hex(hex.x, hex.y).center()
        const points = Hex(hex).toPoint()

        let centeredPoints;
        if (obj.shape === 'circle') {
            centeredPoints = {
                x: points.x + center.x,
                y: points.y + center.y
            }
        } else {
            centeredPoints = getCenters({...obj, x: points.x + center.x, y: points.y + center.y })
        }

        updateShape({
            ...obj,
            ...centeredPoints,
            xHex: hex.x <= 0 ? 0 : hex.x,
            yHex: hex.y <= 0 ? 0 : hex.y,
        })

        e.target.to({
            ...centeredPoints,
            duration: 0.5,
            easing: Konva.Easings.ElasticEaseOut,
            opacity: OPACITY,
            scaleX: 1,
            scaleY: 1
        });
    }


    const renderCircle = (obj) => {
        return (<>
            <Circle
                x={obj.x}
                y={obj.y}
                radius={getSize(obj.size)}
                fill={FILL}
                opacity={OPACITY}
                draggable={true}
                onDragEnd={(e) => handleDragEnd(e, obj)}
                onDragStart={handleDragStart}
                onMouseEnter={handleShowLabelId(obj.id)}
                onMouseOut={handleShowLabelId(null)}
            />
            {renderLabel(obj, showLabelId)}

        </>)
    }

    const renderTriangle = (obj) => {
        return (<RegularPolygon
            sides={3}
            fill={FILL}
            opacity={OPACITY}
            draggable={true}
            onDragEnd={(e) => handleDragEnd(e, obj)}
            onDragStart={handleDragStart}
            {...getTriangleProps(obj)}
        />)
    }

    const renderRectangle = (obj) => {
        return <Rect
            fill={FILL}
            opacity={OPACITY}
            draggable={true}
            onDragEnd={(e) => handleDragEnd(e, obj)}
            onDragStart={handleDragStart}
            {...getRectProps(obj)}
        />
    }

    const renderHexagon = (obj) => {
        return (<RegularPolygon
            sides={6}
            radius={getSize(obj.size)}
            fill={FILL}
            draggable={true}
            opacity={OPACITY}
            onDragEnd={(e) => handleDragEnd(e, obj)}
            onDragStart={handleDragStart}
            {...getHexProps(obj)}
        />)
    }

    return getShapeComponent(obj)
}
