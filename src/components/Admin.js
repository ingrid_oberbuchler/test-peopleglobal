import React from 'react'
import { useForm, useFieldArray } from 'react-hook-form'
import { orderBy } from 'lodash'

export const Admin = (props) => {
    const {onSubmitForm} = props;
    const { register, control, handleSubmit, errors, clearError, watch } = useForm({
        defaultValues: {
            form: [{ label: "", shape: "ellipse", order: ""}]
        }
    });
    const { fields, append, remove } = useFieldArray({control, name: "form"});
    const watchAll = watch();
    
    const onSubmit = data => {
        onSubmitForm(orderBy(data.form, 'order'));
    }

    const addNewFormItems = () => {
        append({ label: "", shape: "", order: "" });
    }
    const removeNewFormItems = (index) => {
        remove(index);
    }

    const renderErrors = () => {
        const err = Object.values(errors)[0];
        if (err && err[0]) {
            switch (Object.values(err[0])) {
                case 'pattern':
                    return <div className="error">Should be number</div>
                    break;
                case 'value' :
                    return <div className="error"> Order ID already exist</div>
                default:
                    break;
            }
        }
        return null;
    };

    // Validate order number - should be uniqe
    const isOrderInputValid = (id) => ({
        required: true,
        pattern: /([1-9][0-9]*)|0/,
        validate: value => {
            const filtered = Object.values(watchAll).filter(x => x === value)
            clearError();
            return filtered.length === 1
        }
    });

    const errorClass = (name) => {
        return errors && Object.keys(errors)[0] === name ? 'error' : '';
    };

    return (
        <div className="admin">
            <form onSubmit={handleSubmit(onSubmit)}>
                {fields.map((item, index) => {
                    return (
                        <div key={item.id} className="formRow">
                            <div>
                                <input type="text" placeholder="Label" name={`form[${index}].label`} defaultValue={`${item.label}`} ref={register({ required: true })} className={errorClass('title')} />
                                <input type="text" placeholder="Order" name={`form[${index}].order`} defaultValue={`${item.order}`} ref={register(isOrderInputValid(index))} className={errorClass('order')} />
                                <select name={`form[${index}].shape`} defaultValue={`${item.shape}`} ref={register({ required: true })} className={errorClass('shape')}>
                                    <option value="ellipse">Ellipse</option>
                                    <option value="circle">Circle</option>
                                    <option value="star">Star</option>
                                    <option value="triangle">Triangle</option>
                                    <option value="database">Database</option>
                                    <option value="box">Box</option>
                                    <option value="diamond">Diamond</option>
                                </select>
                                <div className="errors">{renderErrors()}</div>
                            </div>

                            {index > 0 &&<div>
                                <input type="button" value="Delete" onClick={() => removeNewFormItems(index)} className="button delete"/>
                            </div>}
                            
                        </div>
                    )})
                }
                <div className="formFooter">
                    <input type="submit" value="Save" />
                    <input type="button" value="Add +" onClick={() => addNewFormItems()} className="button add"/>
                </div>
            </form>

        </div>
    )
}
