import React from "react";

import hexagon from './img/hexagon.svg';
import circle from './img/circle.svg';
import triangle from './img/triangle.svg';
import rectangle from './img/rectangle.svg';
import './styles.css'

const initShapes = [
    {img: hexagon, name: 'hexagon'},
    {img: circle, name: 'circle'},
    {img: triangle, name: 'triangle'},
    {img: rectangle, name: 'rectangle'}
]

export const DragDropShapes = ({onDrop}) => {
    return (
        <>
            New:
            {initShapes.map((x, i) => (
                <div className="dragShape" key={i}>
                    {Array(3).fill().reverse().map((shape, j) => (
                        <div className={`dragItem size${j+1} ${j < 2 ? 'hover' : ''}`} key={`${x.name}+${j}`}>
                            <img src={x.img}
                                 draggable={true}
                                 alt=""
                                 onDragStart={() => {
                                     onDrop({size: j+1, shape: x.name})
                                 }}
                            />
                            <span className="number hover">{j+1}</span>
                        </div>
                    ))}
                </div>
            ))}
        </>
    )
}